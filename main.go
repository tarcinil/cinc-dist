//
// Copyright:: Copyright 2019, Cinc Project
// Author: Vern Burton <me@vernburton.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package main

//go:generate go run github.com/chef/go-libs/distgen dist/dist.go dist https://gitlab.com/tarcinil/cinc-dist/-/raw/master/cinc-dist.json
