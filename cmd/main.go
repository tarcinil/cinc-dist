package main


import (
	"fmt"
	dist "gitlab.com/tarcinil/cinc-dist/dist"
	)

func main() {
	fmt.Printf("Global Foo variable: '%s'\n", dist.ClientProduct)
	fmt.Printf("Local Exec variable: '%s'\n", dist.SoloExec)
}